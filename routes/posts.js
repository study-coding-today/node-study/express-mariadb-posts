let express = require('express');
let router = express.Router();
let mysql2 = require('mysql2');
let dbPool = mysql2.createPool({
    host: 'localhost',
    user: 'jgh',
    password: '123456',
    database: 'board',
    connectionLimit: 30,
    waitForConnections: true
});

router.use(express.json());
router.use(express.urlencoded());

// Index
router.get('/', (req, res) => {
    dbPool.query('SELECT * FROM epost ORDER BY createAT DESC', (err, results) => {
        if (err) {
            res.json(err);
        }
        // console.dir(results);
        res.render('posts/index', {results: results});
    });
});

// New
router.get('/new', (req, res) => {
    res.render('posts/new');
});

// Create
router.post('/', (req, res) => {
    dbPool.query('INSERT INTO epost SET title=?, body=?, createAt=?', [req.body.title, req.body.body, new Date()], (err, result) => {
        if (err) {
            return res.json(err);
        }
        res.redirect('/posts');
    });
});

// Show
router.get('/:idx', (req, res) => {
    dbPool.query('SELECT * FROM epost WHERE seq=?', [req.params.idx], (err, result) => {
        if (err) {
            return res.json(err);
        }
        res.render('posts/show', {result: result[0]});
    });
});

// Edit
router.get('/:idx/edit', (req, res) => {
    dbPool.query('SELECT * FROM epost WHERE seq=?', [req.params.idx], (err, result) => {
        if (err) {
            return res.json(err);
        }
        res.render('posts/edit', {result: result[0]});
    });
});

// update
router.post('/:idx/update', (req, res) => {
    dbPool.query('UPDATE epost SET title=?, body=?, updateAt=?', [req.body.title, req.body.body, new Date()], (err, result) => {
        if (err) {
            return res.json(err);
        }
        res.redirect('/posts/'+ req.params.idx);
    });
});

// Destroy
router.post('/:idx/delete', (req, res) => {
    console.log(req.params.idx);
    dbPool.query('DELETE FROM epost WHERE seq=?', [req.params.idx], (err, result) => {
        if (err) {
            return res.json(err);
        }
        res.redirect('/posts');
    });
});

module.exports = router;