// module imports
let express = require('express');
let app = express();

// settings
app.use(express.json());
app.use(express.urlencoded());
app.use(express.static(__dirname + '/statics'));
app.set('view engine', 'ejs');

// Routes
app.use('/', require('./routes/home'));
app.use('/posts', require('./routes/posts'));

// Port Setting
app.listen(3000, () => {
    console.log('서버 3000번 포트가 준비 되었습니다!')
});
